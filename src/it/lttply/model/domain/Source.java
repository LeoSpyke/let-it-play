package it.lttply.model.domain;

/**
 * This enumerator provides a common identifier for the source: it permits to
 * understand what kind of source you are using.
 */
public enum Source {
    /**
     * Sources.
     */
    DATABASE, SECONDARY
}
