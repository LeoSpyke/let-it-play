# Let-IT-Play: your personal, reliable and open-source media manager

## Overview

Over the years, with the concept of "digital life", every human being gets in touch with a lot of "media" starting from Movies to TV Series, Songs and Pictures. 

Since that the amount of those media will grow exponentially during the years, the best solution is adopting a software that helps to manage those important pieces of your life.

Let-IT-Play lives exactly with this goal: it has an internal complicated organization but an external intuitive representation, that helps you find your way in the middle of your "media-jungle"!


## Libraries

- [Annotation for FindBugs](_)
- [Apache Commons CLI](http://commons.apache.org/proper/commons-cli/)
- [Apache Commons I/O](https://commons.apache.org/proper/commons-io/)
- [Apache Commons Lang](http://commons.apache.org/proper/commons-lang/)
- [ControlsFX](http://fxexperience.com/controlsfx/) - JavaFX UI Controls
- [Enzo](https://bitbucket.org/hansolo/enzo/wiki/Home) - JavaFX UI Controls
- [FFmpeg](https://github.com/bramp/ffmpeg-cli-wrapper) - FFmpeg wrapper for Java
- [GUAVA](https://github.com/google/guava) - Google Core Libraries for Java
- [JavaFX](http://docs.oracle.com/javase/8/javafx/get-started-tutorial/jfx-overview.htm#JFXST784)
- [JANSI](http://fusesource.github.io/jansi/) - ANSI escape sequences to format console output 
- [JCDP](https://github.com/dialex/JCDP) - Java Colored Debug Printer
- [JFoenix](https://github.com/jfoenixadmin/JFoenix) - Material Design for JavaFX
- [JNA](https://github.com/java-native-access/jna) - Java Native Access
- [Joda Time](http://www.joda.org/joda-time/) - Quality replacement for the Java date and time classes
- [Logback](https://logback.qos.ch/) - Successor to the popular log4j project
- [JSON](https://mvnrepository.com/artifact/org.json/json) - JSON Serialization and Parsing
- [SLF4J](https://www.slf4j.org/) - Simple Logging Facade for Java
- [SQLite-JDBC](https://bitbucket.org/xerial/sqlite-jdbc/overview) - Accessing and creating SQLite instances in Java
- [VLCJ](https://github.com/caprica/vlcj) - Embedded VLC


## License
This program was originally born to be developed as a university project, now is developed for hobby.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Developers

- Papini Leonardo (IT)
- Grossi Giulio (IT)